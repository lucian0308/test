<?php

namespace Blog\BaseBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Blog\BaseBundle\Entity\Subscriber;
use Blog\BaseBundle\Form\SubscriberType;

/**
 * Subscriber controller.
 *
 * @Route("/subscriber")
 */
class SubscriberController extends Controller
{
    /**
     * Lists all Subscriber entities.
     *
     * @Route("/", name="subscriber")
     * @Method("GET")
     * @Template()
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('BlogBaseBundle:Subscriber')->findAll();

        return array(
            'entities' => $entities,
        );
    }

    /**
     * Creates a new Subscriber entity.
     *
     * @Route("/", name="subscriber_create")
     * @Method("POST")
     * @Template("BlogBaseBundle:Subscriber:new.html.twig")
     */
    public function createAction(Request $request)
    {
        $entity  = new Subscriber();
        $form = $this->createForm(new SubscriberType(), $entity);
        $form->bind($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('subscriber_show', array('id' => $entity->getId())));
        }

        return array(
            'entity' => $entity,
            'form'   => $form->createView(),
        );
    }

    /**
     * Displays a form to create a new Subscriber entity.
     *
     * @Route("/new", name="subscriber_new")
     * @Method("GET")
     * @Template()
     */
    public function newAction()
    {
        $entity = new Subscriber();
        $form   = $this->createForm(new SubscriberType(), $entity);

        return array(
            'entity' => $entity,
            'form'   => $form->createView(),
        );
    }

    /**
     * Finds and displays a Subscriber entity.
     *
     * @Route("/{id}", name="subscriber_show")
     * @Method("GET")
     * @Template()
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('BlogBaseBundle:Subscriber')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Subscriber entity.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return array(
            'entity'      => $entity,
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
     * Displays a form to edit an existing Subscriber entity.
     *
     * @Route("/{id}/edit", name="subscriber_edit")
     * @Method("GET")
     * @Template()
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('BlogBaseBundle:Subscriber')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Subscriber entity.');
        }

        $editForm = $this->createForm(new SubscriberType(), $entity);
        $deleteForm = $this->createDeleteForm($id);

        return array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
     * Edits an existing Subscriber entity.
     *
     * @Route("/{id}", name="subscriber_update")
     * @Method("PUT")
     * @Template("BlogBaseBundle:Subscriber:edit.html.twig")
     */
    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('BlogBaseBundle:Subscriber')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Subscriber entity.');
        }

        $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createForm(new SubscriberType(), $entity);
        $editForm->bind($request);

        if ($editForm->isValid()) {
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('subscriber_edit', array('id' => $id)));
        }

        return array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
     * Deletes a Subscriber entity.
     *
     * @Route("/{id}", name="subscriber_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, $id)
    {
        $form = $this->createDeleteForm($id);
        $form->bind($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('BlogBaseBundle:Subscriber')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find Subscriber entity.');
            }

            $em->remove($entity);
            $em->flush();
        }

        return $this->redirect($this->generateUrl('subscriber'));
    }

    /**
     * Creates a form to delete a Subscriber entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id)
    {
        return $this->createFormBuilder(array('id' => $id))
            ->add('id', 'hidden')
            ->getForm()
        ;
    }
}
