<?php

namespace Blog\BaseBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class PostType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('title')
            ->add('post')
            ->add('status', 'entity', array(
                'class'=>'BlogBaseBundle:Status', 
                'property'=>'name', 
                'multiple' => false, 
                'expanded' => false
            ))
            ->add('cagetory', 'entity', array(
                'class'=>'BlogBaseBundle:Category', 
                'property'=>'name', 
                'multiple' => false, 
                'expanded' => true
            ))
            ->add('file')
        ;
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Blog\BaseBundle\Entity\Post'
        ));
    }

    public function getName()
    {
        return 'blog_basebundle_posttype';
    }
}
